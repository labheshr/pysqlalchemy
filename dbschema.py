'''
Defines the dw_loans Database schema and handler to the Database.
This code uses SQLAlchemy as an ORM, so any SQL based database can be used with this code.
Here we use sqllite.
To use another db, you'll have to configure its connection details in Db class's constructor
'''
from sqlalchemy import Column, Date, String, Integer, Float, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from konstants import DB_NAME

Base = declarative_base()

class Loans(Base):
    '''
    loans schema
    '''
    __tablename__ = 'dw_loans'
    id = Column(Integer, primary_key=True)
    loan_id = Column(Integer)
    issue_dt = Column(Date)
    pmt_date =  Column(Date)
    fees  = Column(Float)
    opening_bal = Column(Float)
    writedowns = Column(Float)
    principal_paid = Column(Float)
    sch_principal_paid = Column(Float)
    unsch_principal_paid = Column(Float)
    end_bal = Column(Float)
    pmt_due = Column(Float)
    principal_due = Column(Float)
    interest_due = Column(Float)
    interest_paid = Column(Float)
    hist_interest_shortfall = Column(Float)
    duration = Column(Float)
    min_coupon = Column(Float)
    future_interest_shortfall = Column(Float)
    open_fico = Column(Integer)
    last_fico = Column(Integer)
    interest_rate = Column(Float)
    term = Column(Integer)
    is_writedown = Column(Boolean)
    grade = Column(String(5))
    status = Column(String(50))
    loan_amt = Column(Float)
    interest_payment = Column(Float)
    sub_grade = Column(String(5))
    emp_title = Column(String(50))
    emp_length = Column(String(30))
    home_own = Column(String(30))
    annual_income = Column(Float)
    income_verified = Column(String(50))
    pmt_plan = Column(String(5))
    purpose = Column(String(100))
    title = Column(String(100))
    zipcode = Column(String(5))
    state = Column(String(2))
    dti =  Column(Float)
    delq_past_2y = Column(Integer)
    inq_past_6m  = Column(Integer)
    mths_since_last_delinq = Column(Integer)
    mths_since_last_req = Column(Integer)
    open_accts = Column(Integer)
    pub_recs = Column(Integer)
    rev_bal = Column(Float)
    rev_util = Column(Float)
    total_accts = Column(Integer)
    coll_last_12m = Column(Integer)
    mths_since_major_derog = Column(Integer)
    pol_code = Column(Integer)
    #new columns should be appended at the end


class Db( object ):
    '''
    create a session to the db
    '''
    def __init__(self, dbenv='InMemory'):
        if dbenv == 'InMemory':
            engine = create_engine(DB_NAME)
            Base.metadata.create_all(engine)
            self.sessionMaker = sessionmaker(bind=engine)
        else:
            raise 'no other dbenv than in memory sqllite configured as yet'
        
    
    def session(self):
        return self.sessionMaker()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
       
    

