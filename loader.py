'''
Defines all logic to load data into dw_loans table.
Loads accomodate:
1) Parsing a CSV file to get the data to be loaded (assumes that CSV file is "relatively clean" i.e. we dont  have numbers in place of dates and headers are well defined and consistent.
   If you want a stricter validation(This will obviously slow down the load process), use validator.py: CSVValidator.validate(<<input_as_dict>>)
2) Manages transactional loads/rollbacks i.e. all or nothing is loaded into the db
3) Allows for a means to extend the load process to a source that is_not_CSV
NOTE: this code **removes** prev data for the same payment date if its reloaded. Otherwise we'll have duplicates in the db
'''
import csv
import abc
#from validator import CSVValidator 
import logging
from dbschema import Loans,Db
import dateutil.parser
from konstants import CSV_DATATYPES, CSV_FLDS_TO_DB_FLDS, HANDLER_TYPE
import traceback

class BaseLoader(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def _getDataFromSource(self, src):
        raise NotImplementedError('Subclass must implement this function')
    
    @abc.abstractmethod
    def _argsToDbFlds(self, kvpair):
        raise NotImplementedError('Subclass must implement this function')
    
    @abc.abstractmethod
    def load(self):
        raise NotImplementedError('Subclass must implement this function')

class CSVLoader(BaseLoader):
    '''
    given a csv data file, validate and load data into database
    '''
    def __init__(self,src, paymentDate):
        
        self.source = src
        self.paymentDate = paymentDate
        self.csvFldsToDbFlds = CSV_FLDS_TO_DB_FLDS
        
    def _argsToDbFlds(self,kvpair):
        '''
        maps b/w csv fields to database fields
        '''
        return dict( (self.csvFldsToDbFlds[k],v) for k,v in kvpair.iteritems() )
        
    
    def _getDataFromSource(self):
        '''
        this expects the **absolute** path to the csv source file which has the headers
        '''
        with open(self.source,mode='rU') as infile:
            reader =  csv.reader(infile)
            for line in reader:
                yield line

    def _formatInputs(self,kvpair):
        #any formatting you need to do to the input data
        for k,v in kvpair.iteritems():
            v = v.strip()
            if k == 'RevUtil':
                try:
                    kvpair[k] = float( kvpair[k].replace('%','') )/100
                except:
                    kvpair[k] = float(0)
            elif k in ('IssueDt', 'PmtDate'):
                kvpair[k] = dateutil.parser.parse( kvpair[k] ).date()
            elif k == 'DefInd':
                kvpair[k] = bool( int(kvpair[k]) )
            else:
                try:
                    kvpair[k] = CSV_DATATYPES[k]( v.replace(',','') ) #as a part of cleanup, replace all numformats from 123,123 to 123123
                except:
                    if CSV_DATATYPES[k] == unicode:
                        kvpair[k] = unicode('')
                    else:
                        kvpair[k] = CSV_DATATYPES[k](0)
                    

    def load(self):
        data = enumerate(self._getDataFromSource())
        _, hdr = data.next()
        header = [ x.strip() for x in hdr ] #remove trailing spaces
        #now validate and load data
        session =  Db(dbenv='InMemory').session()
        try:
            #find any data for this payment date if it exists in the db and delete it before reloading
            session.query(Loans).filter(Loans.pmt_date == self.paymentDate).delete(synchronize_session=False)
            for _idx, d in data:
                keyedData = dict( zip(header,d) )
                self._formatInputs(keyedData) #this is passed by reference, so we should be good
                mappedArgs = self._argsToDbFlds(keyedData)
                loan = Loans(**mappedArgs)
                session.add(loan)
            session.commit()
        except:
            logging.error(traceback.format_exc())
            session.rollback()
                

class LoaderFactory(object):
    '''
    create an extendible factory to load data from various sources. 
    (At the moment we have only CSV as a defined source)
    '''
    sourceSelector = { HANDLER_TYPE.CSV: CSVLoader, }
    
    @classmethod
    def create(cls, srcType):
        loaderCls = cls.sourceSelector.get(srcType, BaseLoader)
        return loaderCls
            
    
    
