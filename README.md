# README #

This software provides an API to store and retrieve collateral based information from a sql based database

### Details ###

* Use runner.py as the entry point for the code. All user APIs are exposed here
* sample_queries.py to see how to write queries using SQLAlchemy against the dw_loans database
* Currently uses sqlalchemy as the underlying in memory database
* All dependencies are listed in requirements.txt

###TODO###
* Add more unit testcases


### Who do I talk to? ###

* labheshr[at]gmail[dot]com for any inquiry