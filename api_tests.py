'''
This module holds end to end API tests (as defined in the runner).
This can be used to test db loads/extracts.

The calls here can be emulated by a cron job to run conditionally/periodically
'''
import runner
from konstants import HANDLER_TYPE
import datetime

def test_Extract():
    inputs = {'trade_date':datetime.date(2015,3,1), 'last_payment_date':datetime.date(2015,1,1), 'current_payment_date':datetime.date(2015,2,1),
              'portfolio_orig_bal': 682230820.0,'original_tranche_notional':  50000000.0, 
              'original_credit_enhancement': 0.2, 'coupon': 0.05 }
    pay_dts = [ datetime.date(2015,1,1),datetime.date(2014,1,1), datetime.date(2014,2,1)]
    runner.Extract(HANDLER_TYPE.CSV, '/Users/Labhesh/Downloads/peeriqfiles/testoutput/', inputs, paymentDates=pay_dts)

def test_LoadDb():
    runner.LoadDb( HANDLER_TYPE.CSV, '/Users/Labhesh/Downloads/peeriqfiles/mar14.csv', datetime.date(2014,3,1) )

'''
uncomment the below to run as a test
'''
test_Extract()
#test_LoadDb()
