'''
This module defines the APIs/Interface for:
1) Loading the database
2) Extracting the data out of database
'''
from loader import LoaderFactory
from outbound import ExtractorFactory
import logging
import sys

'''
configure logging so everything logs at std. output
'''
root = logging.getLogger()
root.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
root.addHandler(ch)

def LoadDb(srcType, src, paymentDate):
    '''
    Parameters
    ----------
    
    srcType: what is the source of the data being loaded. Only sources defined in HANDLER_TYPE are allowed
    src: the actual source of data. At the moment only the absolute location of a csv file is allowed.Can be extended to another db cursor etc
    paymentDate: the payment date you'd like to load the data for
    
    Example
    -------
    
    >> from konstants import HANDLER_TYPE
    >> import datetime
    >> LoadDb(HANDLER_TYPE.CSV, '/Users/Labhesh/Downloads/peeriqfiles/jan15.csv', datetime.date(2015,1,1))
    '''
    logging.info('Start<--Loading dw_loans database')
    loaderCls = LoaderFactory.create(srcType)
    loaderObj = loaderCls(src, paymentDate)
    loaderObj.load()
    logging.info('End<--Loading dw_loans database')

def Extract(srcType, outputinfo, inputs, paymentDates=None):
    '''
    Parameters
    ----------
    srcType: What kind of output do you want? such as CSV, dict etc. Only sources defined in HANDLER_TYPE are allowed
    outputinfo: Where do you want to persist data? For CSV: this could be a location on your harddisk
    inputs: Any custom inputs (data structure: dict) in addition to data in the database to do any calcs etc which will be used in the output
    paymentDates: a list of payment dates you would like to get the data for, if None: it returns data for all payment dates in the db
    
    The output is dependent on how the formatOuput function of extract class is defined. In case of CSV, we get:
    1) asset.csv
    2) liability.csv
    3) summary_<<for_each_payment_date>>.csv
    
    Example
    -------
    
    >> from konstants import HANDLER_TYPE
    >> import datetime
    >> inputs = {'trade_date':datetime.date(2015,3,1), 'last_payment_date':datetime.date(2015,1,1), 'current_payment_date':datetime.date(2015,2,1),
          'portfolio_orig_bal': 682230820.0,'original_tranche_notional':  50000000.0, 
          'original_credit_enhancement': 0.2, 'coupon': 0.05  }
    >> pay_dts = [ datetime.date(2015,1,1),]
    >> Extract(HANDLER_TYPE.CSV, '/Users/Labhesh/Downloads/peeriqfiles/', inputs, paymentDates=pay_dts)
    '''
    logging.info('Start<--Extracting data from dw_loans db')
    extractorCls = ExtractorFactory.create(srcType)
    extractorObj  = extractorCls(outputinfo,inputs,paymentDates)
    extractorObj.formatOutput()
    logging.info('End<--Extracting data from dw_loans db')