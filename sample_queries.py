'''
This module demonstrates sample database queries to get writedowns, or fully paid off loans. Can be extended to get any sort of info. from the db
'''
from dbschema import Db, Loans

def getWriteDownCount():
    '''
    get the count of written off loans
    '''
    session = Db(dbenv='InMemory').session()
    print session.query(Loans.loan_id).filter(Loans.is_writedown==1).count()


def getWriteDownSamples():
    '''
    get the first 10 written down loans
    '''
    session = Db(dbenv='InMemory').session()
    query = session.query(Loans.loan_id, Loans.is_writedown, Loans.pmt_date).filter(Loans.is_writedown==1).limit(10)
    for q in query:
        print q

def getFullyPaidLoanSamples():
    '''
    get the first 10 samples where the loan is fully paid off
    '''
    session = Db(dbenv='InMemory').session()
    query = session.query(Loans.loan_id, Loans.end_bal, Loans.pmt_date).filter(Loans.end_bal==0).limit(10)
    for q in query:
        print q
    

'''
sample queries to filter on dw_loans data; uncomment to run
'''
#getWriteDownCount()
#getWriteDownSamples()
#getFullyPaidLoanSamples()