'''
Define any constants associated with the code base at one place
'''
import datetime
DB_NAME = 'sqlite:///loansdb.sqlite'
CSV_DATATYPES = { 'LoanID': int, 'IssueDt': datetime.date, 'PmtDate': datetime.date, 'Fees': float,'OpenBal':float,'Writedowns':float,
                         'PrinPaid': float, 'SchPrinPaid':float, 'UnschPrinPaid': float, 'EndBal':float, 'PmtDue':float, 
                         'PrinDue':float, 'IntDue':float, 'IntPaid':float, 'HistIntShortFall':float, 'Duration':float, 'MinCoup':float,
                         'FutIntShortFall':float, 'OpenFICO':int, 'LastFICO':int, 'IntRate':float, 'Term':int, 'DefInd':bool, 'Grade':unicode,
                         'Status':unicode, 'LoanAmt':float, 'InstPmt':float, 'SubGrade':unicode, 'EmpTitle':unicode, 'EmpLength':unicode, 'HomeOwn':unicode, 
                         'AnnInc':float, 'IncVerf':unicode, 'PmtPlan':unicode, 'Purpose':unicode, 'Title':unicode, 'ZipCode':unicode, 'State':unicode, 'DTI':float,
                         'DelqPast2Y':int, 'InqPast6m':int, 'MnthsSinceLastDelq':int, 'MnthsSinceLastRec':int, 'OpenAccts':int, 'PubRecs':int,
                         'RevBal':float, 'RevUtil':float, 'TotalAccts':int, 'CollLast12m':int, 'MnthsSinceMajDerog':int, 'PolCode':int,}

CSV_FLDS_TO_DB_FLDS = { 'LoanID': 'loan_id', 'IssueDt': 'issue_dt', 'PmtDate': 'pmt_date', 'Fees': 'fees','OpenBal':'opening_bal','Writedowns':'writedowns',
                                 'PrinPaid': 'principal_paid', 'SchPrinPaid':'sch_principal_paid', 'UnschPrinPaid': 'unsch_principal_paid', 'EndBal':'end_bal', 'PmtDue':'pmt_due', 
                                 'PrinDue':'principal_due', 'IntDue':'interest_due', 'IntPaid':'interest_paid', 'HistIntShortFall':'hist_interest_shortfall', 'Duration':'duration', 'MinCoup':'min_coupon',
                                 'FutIntShortFall':'future_interest_shortfall', 'OpenFICO':'open_fico', 'LastFICO':'last_fico', 'IntRate':'interest_rate', 'Term':'term', 'DefInd':'is_writedown', 'Grade':'grade',
                                 'Status':'status', 'LoanAmt':'loan_amt', 'InstPmt':'interest_payment', 'SubGrade':'sub_grade', 'EmpTitle':'emp_title', 'EmpLength':'emp_length', 'HomeOwn':'home_own', 
                                 'AnnInc':'annual_income', 'IncVerf':'income_verified', 'PmtPlan':'pmt_plan', 'Purpose':'purpose', 'Title':'title', 'ZipCode':'zipcode', 'State':'state', 'DTI':'dti',
                                 'DelqPast2Y':'delq_past_2y', 'InqPast6m':'inq_past_6m', 'MnthsSinceLastDelq':'mths_since_last_delinq', 'MnthsSinceLastRec':'mths_since_last_req', 'OpenAccts':'open_accts', 'PubRecs':'pub_recs',
                                 'RevBal':'rev_bal', 'RevUtil':'rev_util', 'TotalAccts':'total_accts', 'CollLast12m':'coll_last_12m', 'MnthsSinceMajDerog':'mths_since_major_derog', 'PolCode':'pol_code',}


class HANDLER_TYPE(object):
    CSV = 1
