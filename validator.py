from voluptuous import Schema
import traceback
import logging
from konstants import CSV_DATATYPES

class CSVValidator(object):
    '''
    uses voluptuous to validate csv input
    '''
    schema = Schema( CSV_DATATYPES )
    
    @classmethod
    def validate(cls,toValidate):
        status = True
        try:
            cls.schema(toValidate)
        except:
            status = False
            logging.error(traceback.format_exc())
        finally:
            return status
            
    