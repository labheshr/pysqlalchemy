'''
This module defines all logic associated with extracting data, performing derived data calculations and formatting results per the output requirements
'''

from sqlalchemy.sql import func
from dbschema import Db, Loans
import dateutil.relativedelta
import os
import logging
from konstants import HANDLER_TYPE
import datetime

class BaseExtractor(object):
    '''
    Base class for:
    1) getting data out of database
    2) performing any derived calcs on the data
    '''
    def __init__( self,outputSrc, customInputs={}, paymentDates=None ):
        self.customInputs = customInputs
        self.outputSrc = outputSrc
        self.paymentDates = paymentDates
        self.session = Db(dbenv='InMemory').session()
        
    def queryDbByPaymentDates(self, pmtDates=None):
        pmtDates = pmtDates or self.paymentDates
        if pmtDates:
            result = self.session.query(Loans.pmt_date, func.sum(Loans.opening_bal).label('Portfolio_Beginning_Balance'),
                       func.sum(Loans.writedowns).label('Portfolio_Writedowns'),
                       func.sum(Loans.principal_paid).label('Portfolio_Principal_Pay'),
                       func.sum(Loans.hist_interest_shortfall).label('Hist_Int_Shortfall'), #needed to compute Portfolio Interest Shortfall
                       func.sum(Loans.future_interest_shortfall).label('Fut_Int_Shortfall'), #needed to compute Portfolio Interest Shortfall
                       ).filter( getattr(Loans, 'pmt_date').in_(pmtDates) ).group_by(Loans.pmt_date).all()
        else:
            #If no payment dates provided, get everything grouped by payment dates
            result = self.session.query(Loans.pmt_date, func.sum(Loans.opening_bal).label('Portfolio_Beginning_Balance'),
                       func.sum(Loans.writedowns).label('Portfolio_Writedowns'),
                       func.sum(Loans.principal_paid).label('Portfolio_Principal_Pay'),
                       func.sum(Loans.hist_interest_shortfall).label('Hist_Int_Shortfall'), #needed to compute Portfolio Interest Shortfall
                       func.sum(Loans.future_interest_shortfall).label('Fut_Int_Shortfall'), #needed to compute Portfolio Interest Shortfall
                       ).group_by(Loans.pmt_date).all()
        
        return result

    def _getPrevSOM(self, pmtDate):
        '''
        Given a current payment date, get the previous payment date as a start of month
        '''
        return pmtDate + dateutil.relativedelta.relativedelta(months=-1)
        

    def _generateRawOutput(self):
        '''
        Perform all calcs using database results and returns a dict output with key=payment date, value = all calculation results
        '''
        #first get query results
        outputByPmtDate = dict() #this contains the output by payment date
        queryResult = self.queryDbByPaymentDates()
        for qr in queryResult:
            #assets
            pfolio_interest_shortfall = qr.Hist_Int_Shortfall + qr.Fut_Int_Shortfall
            pfolio_ending_bal = qr.Portfolio_Beginning_Balance - qr.Portfolio_Writedowns - qr.Portfolio_Principal_Pay
            prev_pfolio_factor = qr.Portfolio_Beginning_Balance/self.customInputs['portfolio_orig_bal']
            curr_pfolio_factor = pfolio_ending_bal/qr.Portfolio_Beginning_Balance
            
            #get previous payment month's writedowns and total portfolio pays
            prevSOM = self._getPrevSOM(qr.pmt_date)
            prevMnthQueryRes = self.queryDbByPaymentDates( [prevSOM] )
            prev_total_portfolio_writedowns = 0.
            prev_total_pfolio_principal_pay = 0.
            if len(prevMnthQueryRes) == 1:
                pmqr = prevMnthQueryRes[0]
                prev_total_portfolio_writedowns = pmqr.Portfolio_Writedowns
                prev_total_pfolio_principal_pay = pmqr.Portfolio_Principal_Pay

            total_curr_pfolio_writedowns = qr.Portfolio_Writedowns + prev_total_portfolio_writedowns
            current_total_pfolio_principal_pay = qr.Portfolio_Principal_Pay + prev_total_pfolio_principal_pay
            
            #liabilities
            prev_tranche_notional = prev_pfolio_factor * self.customInputs['original_tranche_notional']
            tranche_writedowns = 0.
            if prev_tranche_notional < 0:
                tranche_writedowns = self.customInputs['original_tranche_notional'] * qr.Portfolio_Writedowns/self.customInputs['portfolio_orig_bal']
            tranche_interest_shortfall = self.customInputs['original_tranche_notional'] * pfolio_interest_shortfall/self.customInputs['portfolio_orig_bal']
            tranche_principal_pay = ( prev_tranche_notional/ (1-self.customInputs['original_credit_enhancement']) ) * (qr.Portfolio_Principal_Pay/qr.Portfolio_Beginning_Balance)
            current_notional = prev_tranche_notional - tranche_writedowns - tranche_principal_pay
            prev_credit_enhancement = self.customInputs['original_credit_enhancement'] - (prev_total_portfolio_writedowns/self.customInputs['portfolio_orig_bal'])
            curr_credit_enhancement = prev_credit_enhancement - (qr.Portfolio_Writedowns/self.customInputs['portfolio_orig_bal'])
            total_tranche_writedowns = 0.
            if prev_credit_enhancement < 0:
                total_tr_w_a = ( total_curr_pfolio_writedowns*(self.customInputs['original_tranche_notional']/(1-self.customInputs['original_credit_enhancement'])/self.customInputs['portfolio_orig_bal'] ))
                total_tr_w_b = ( self.customInputs['original_credit_enhancement'] * self.customInputs['original_tranche_notional']/(1-self.customInputs['original_credit_enhancement']) )
                total_tranche_writedowns = total_tr_w_a - total_tr_w_b
            
            total_tranche_principal_pay = ( self.customInputs['original_tranche_notional'] * current_total_pfolio_principal_pay )/(self.customInputs['portfolio_orig_bal'] * (1-self.customInputs['original_credit_enhancement']) )
            if total_tranche_principal_pay > self.customInputs['original_tranche_notional']:
                total_tranche_principal_pay = self.customInputs['original_tranche_notional']
            
            #CLN
            fixed_protection_buyer_pays = ( self.customInputs['coupon'] * prev_tranche_notional * (self.customInputs['current_payment_date']- self.customInputs['last_payment_date']).days )/360
            fixed_protection_seller_rcvs = fixed_protection_buyer_pays
            float_protection_buyer_rcvs = tranche_writedowns + tranche_interest_shortfall
            float_protection_seller_rcvs = tranche_principal_pay
            '''
            This dict holds the final result
            '''
            outputByPmtDate[qr.pmt_date] = {'trade_date': self.customInputs['trade_date'],
                                            'last_payment_date': self.customInputs['last_payment_date'],
                                            'current_payment_date': self.customInputs['current_payment_date'],
                                            #assets/portfolio
                                            'Portfolio_Original_Balance': self.customInputs['portfolio_orig_bal'],
                                            'Portfolio_Beginning_Balance': qr.Portfolio_Beginning_Balance,
                                            'Portfolio_Writedowns': qr.Portfolio_Writedowns,
                                            'Portfolio_Interest_Shortfall': pfolio_interest_shortfall,
                                            'Portfolio_Principal_Pay' : qr.Portfolio_Principal_Pay, 
                                            'Portfolio_Ending_Balance': pfolio_ending_bal,
                                            'Previous_Portfolio_Factor': prev_pfolio_factor,
                                            'Current_Portfolio_Factor': curr_pfolio_factor,
                                            'Previous_Total_Portfolio_Writedowns' : prev_total_portfolio_writedowns, 
                                            'Previous_Total_Portfolio_Principal_Pay': prev_total_pfolio_principal_pay,
                                            'Total_Current_Portfolio_Writedowns': total_curr_pfolio_writedowns,
                                            'Current_Total_Portfolio_Principal_Pay':current_total_pfolio_principal_pay,
                                            #liabilities/Tranches
                                            'Original_Tranche_Notional': self.customInputs['original_tranche_notional'],
                                            'Original_Credit_Enhancement': self.customInputs['original_credit_enhancement'],
                                            'Previous_Tranche_Notional': prev_tranche_notional,
                                            'Tranche_Writedowns':  tranche_writedowns,
                                            'Tranche_Interest_Shortfall' : tranche_interest_shortfall,
                                            'Tranche_Principal_Pay': tranche_principal_pay,
                                            'Current_Notional' : current_notional,
                                            'Previous_Credit_Enhancement': prev_credit_enhancement,
                                            'Current_Credit_Enhancement': curr_credit_enhancement,
                                            'Total_Tranche_Writedowns': total_tranche_writedowns,
                                            'Total_Tranche_Principal_Pay': total_tranche_principal_pay,
                                            #CLNs
                                            'Coupon': self.customInputs['coupon'],
                                            'Fixed_Protection_Buyer_Pays': fixed_protection_buyer_pays,
                                            'Fixed_Protection_Seller_Rcvs': fixed_protection_seller_rcvs,
                                            'Float_Protection_Buyer_Rcvs': float_protection_buyer_rcvs,
                                            'Float_Protection_Seller_Rcvs': float_protection_seller_rcvs,
                                            #Annualized
                                            'Fixed_Protection_Buyer_Pays_Annualized': fixed_protection_buyer_pays*6,
                                            'Fixed_Protection_Seller_Rcvs_Annualized': fixed_protection_seller_rcvs*6,
                                            'Float_Protection_Buyer_Rcvs_Annualized': float_protection_buyer_rcvs*6,
                                            'Float_Protection_Seller_Rcvs_Annualized': float_protection_seller_rcvs*6,
                                            }
            
        return outputByPmtDate
        

    def formatOutput(self):
        '''
        This formats the output as CSV, JSON etc based on how its implemented by derived class
        '''
        raise NotImplementedError('Subclass must implement this function')


class CSVExtractor(BaseExtractor):
    '''
    extract data in form of a CSV
    Headers used here isolate us from db/or raw output specific names
    '''    
    def formatOutput(self):
        outputByPmtDate = self._generateRawOutput()
        if not os.path.isdir(self.outputSrc):
            logging.error('The output source does not correspond to an existing directory. No formatted output generated')
            return
        #first sort by payment dates
        sortedOutput = sorted(outputByPmtDate.iteritems())
        #make seperate outputs for assets/liabilities/summary
        #First do Asset
        with open(os.path.join(self.outputSrc,'asset_%s.csv'%datetime.datetime.now().isoformat()), 'wb') as f:
            assetHeader = ['Payment Month', 'Portfolio Original Balance', 'Portfolio Beginning Balance',
                           'Portfolio Writedowns', 'Portfolio Interest Shortfall', 'Portfolio Principal Pay',
                           'Portfolio Ending Balance', 'Previous Portfolio Factor(Pct.)', 'Current Portfolio Factor(Pct.)', 
                           'Previous Total Portfolio Writedowns', 'Previous Total Portfolio Principal Pay', 
                           'Total Current Portfolio Writedowns', 'Current Total Portfolio Principal Pay',]
            f.write(','.join(assetHeader)) 
            f.write('\n')
            for s in sortedOutput:
                pmtDate, info = s
                toWrite = [ pmtDate, info['Portfolio_Original_Balance'],info['Portfolio_Beginning_Balance'],
                            info['Portfolio_Writedowns'], info['Portfolio_Interest_Shortfall'], info['Portfolio_Principal_Pay'],
                            info['Portfolio_Ending_Balance'],info['Previous_Portfolio_Factor']*100,
                            info['Current_Portfolio_Factor']*100, info['Previous_Total_Portfolio_Writedowns'], 
                            info['Previous_Total_Portfolio_Principal_Pay'], info['Total_Current_Portfolio_Writedowns'], 
                            info['Current_Total_Portfolio_Principal_Pay'], ]
                f.write(','.join(str(x) for x in toWrite))
                f.write('\n')
        
        #Now do liabilities
        with open(os.path.join(self.outputSrc,'liability_%s.csv'%datetime.datetime.now().isoformat()), 'wb') as f:
            liabilityHeader = ['Payment Month', 'Original Tranche Notional', 'Original Credit Enhancement(Pct.)', 
                           'Previous Tranche Notional', 'Tranche Writedowns', 'Tranche Interest Shortfall', 
                           'Tranche Principal Pay', 'Current Notional', 'Previous Credit Enhancement(Pct.)', 
                           'Current Credit Enhancement(Pct.)', 'Total Tranche Writedowns','Total_Tranche_Principal_Pay',]
            f.write(','.join(liabilityHeader)) 
            f.write('\n')
            for s in sortedOutput:
                pmtDate, info = s
                toWrite = [ pmtDate, info['Original_Tranche_Notional'],info['Original_Credit_Enhancement']*100,
                            info['Previous_Tranche_Notional'],info['Tranche_Writedowns'],
                            info['Tranche_Interest_Shortfall'], info['Tranche_Principal_Pay'], 
                            info['Current_Notional'], info['Previous_Credit_Enhancement']*100,
                            info['Current_Credit_Enhancement']*100, info['Total_Tranche_Writedowns'], 
                            info['Total_Tranche_Principal_Pay'], ]
                f.write(','.join(str(x) for x in toWrite))
                f.write('\n')
    
        #now write the summary - 1 per payment month
        for s in sortedOutput:
            pmtDate, info = s
            with open(os.path.join(self.outputSrc,'summary_%s.csv'%pmtDate.isoformat()), 'wb') as f:
                f.write('Trade Date, %s'%info['trade_date'])
                f.write('\n')
                f.write('Last Payment Date, %s'%info['last_payment_date'])
                f.write('\n')
                f.write('Current Payment Date, %s'%info['current_payment_date'])
                f.write('\n')
                f.write('\n')
                
                f.write('Portfolio')
                f.write('\n')
                f.write('Portfolio Original Balance, %s, Previous Total Portfolio Writedowns, %s'%( info['Portfolio_Original_Balance'], info['Previous_Total_Portfolio_Writedowns']) )
                f.write('\n')
                f.write('Portfolio Beginning Balance, %s, Current Total Portfolio Writedowns, %s'%( info['Portfolio_Beginning_Balance'], info['Total_Current_Portfolio_Writedowns']) )
                f.write('\n')
                f.write('Portfolio Writedowns, %s, Previous Total Portfolio Principal Pay, %s'%( info['Portfolio_Writedowns'], info['Previous_Total_Portfolio_Principal_Pay']) )
                f.write('\n')
                f.write('Portfolio Interest Shortfall, %s, Current Total Portfolio Principal Pay, %s'%( info['Portfolio_Interest_Shortfall'], info['Current_Total_Portfolio_Principal_Pay']) )
                f.write('\n')
                f.write('Portfolio Principle Pay, %s, Previous Portfolio Factor, %s pct.'%( info['Portfolio_Principal_Pay'], info['Previous_Portfolio_Factor']*100) )
                f.write('\n')
                f.write('Portfolio Ending Balance, %s, Current Portfolio Factor, %s pct.'%( info['Portfolio_Ending_Balance'], info['Current_Portfolio_Factor']*100) )
                f.write('\n')
                f.write('\n')

                f.write('Tranche')
                f.write('\n')
                f.write('Original Tranche Notional, %s, Original Credit Enhancement, %s pct.'%( info['Original_Tranche_Notional'], info['Original_Credit_Enhancement']*100) )
                f.write('\n')
                f.write('Previous Tranche Notional, %s, Previous Credit Enhancement, %s pct.'%( info['Previous_Tranche_Notional'], info['Previous_Credit_Enhancement']*100) )
                f.write('\n')
                f.write('Tranche Writedowns, %s, Current Credit Enhancement, %s pct.'%( info['Tranche_Writedowns'], info['Current_Credit_Enhancement']*100) )
                f.write('\n')
                f.write('Tranche Interest Shortfall, %s, Total Tranche Writedowns, %s'%( info['Tranche_Interest_Shortfall'], info['Total_Tranche_Writedowns']) )
                f.write('\n')
                f.write('Tranche Principle Pay, %s, Total Tranche Principal Pay, %s'%( info['Tranche_Principal_Pay'], info['Total_Tranche_Principal_Pay']) )
                f.write('\n')
                f.write('Current Notional, %s'%info['Current_Notional'] )
                f.write('\n')
                f.write('\n')
                
                f.write('Assuming a fully funded CLN Structure')
                f.write('\n')
                f.write('Fixed Payment')
                f.write('\n')
                f.write('Coupon, %s pct.'%(info['Coupon']*100))
                f.write('\n')
                f.write('Protection Buyer Pays, %s, Protection Buyer Pays Annualized, %s'%(info['Fixed_Protection_Buyer_Pays'], info['Fixed_Protection_Buyer_Pays_Annualized']))
                f.write('\n')
                f.write('Protection Seller Receives, %s, Protection Seller Receives Annualized, %s'%(info['Fixed_Protection_Seller_Rcvs'], info['Fixed_Protection_Seller_Rcvs_Annualized']))
                f.write('\n')
                f.write('\n')
                f.write('Floating Payment')
                f.write('\n')
                f.write('Protection Buyer Receives, %s, Protection Buyer Receives Annualized, %s'%(info['Float_Protection_Buyer_Rcvs'], info['Float_Protection_Buyer_Rcvs_Annualized']))
                f.write('\n')
                f.write('Protection Seller Receives, %s, Protection Seller Receives Annualized, %s'%(info['Float_Protection_Seller_Rcvs'], info['Float_Protection_Seller_Rcvs_Annualized']))
                f.write('\n')
                

class ExtractorFactory(object):
    '''
    Factory to call the right Extractor based on HANDLER_TYPE
    '''
    extractSelector= { HANDLER_TYPE.CSV:CSVExtractor, }
    
    @classmethod
    def create(cls, srcType):
        extractorCls = cls.extractSelector.get(srcType, BaseExtractor)
        return extractorCls
        
