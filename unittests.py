'''
Define Sample unit tests for the framework
'''
import unittest2
import datetime
from outbound import BaseExtractor

class Extractor_Tests(unittest2.TestCase):
    
    def setUp(self):
        self.extractorObj = BaseExtractor( 'test', customInputs={},paymentDates=[datetime.date(2015,1,1)] )
    
    def test_queryDbByPaymentDates(self):
        res = self.extractorObj.queryDbByPaymentDates()
        self.assertEquals(1, len(res), 'test case failed, mismatch in expected and actual query result')
    
    def test_getPrevSOM(self):
        res = self.extractorObj._getPrevSOM(datetime.date(2015,1,1))
        self.assertEqual(datetime.date(2014,12,1),res, 'test case failed, should return the prev start of month')